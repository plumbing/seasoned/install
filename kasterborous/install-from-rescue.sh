



# based on https://gist.github.com/ppmathis/ccfbfce86484dc61834c1f17568d7b80
# https://www.schmidp.com/2014/12/12/full-disk-encryption-with-grub-2-+-luks-+-lvm-+-swraid-on-debian/
# https://falkhusemann.de/blog/2013/10/debian-colocationleased-server-full-disk-encryption/
# https://www.maddes.net/software/luks.htm

# targeted at hetzner rescue system

#boot debian live 
sudo su
passwd user
apt update&&apt install -y openssh-server&&systemctl start sshd&&ip a


apt install -y parted mdadm gdisk screen gdisk debootstrap lvm2 cryptsetup 


D1=/dev/sda
D2=/dev/sdb



export D1
export D2



mdadm --stop /dev/md0

# nuke disks

sgdisk --zap-all $D1
sgdisk --zap-all $D2

dd if=/dev/zero of=$D1 count=200 bs=1M
dd if=/dev/zero of=$D2 count=200 bs=1M

partprobe $D1
partprobe $d2


# partition disks
partcmd="parted --script --align optimal $D1"
$partcmd mklabel gpt
$partcmd mkpart primary 2048s 4095s
$partcmd set 1 bios_grub on
$partcmd name 1 "BIOS"
$partcmd mkpart primary 4096s 100%
$partcmd set 2 raid on
$partcmd name 2 "SW-RAID"

sgdisk -R=$D2 $D1
sgdisk -G $D2
sleep 1
partprobe $D1
partprobe $D2

sleep 1
# add md

yes|mdadm --create /dev/md0 --verbose --level=mirror --raid-devices=2 ${D1}2 ${D2}2

# add lvm

pvcreate /dev/md0
vgcreate spinningdisk /dev/md0

lvcreate -n boot -L500M spinningdisk
lvcreate -n root -l 100%FREE spinningdisk


#format /boot

mkfs.ext4 /dev/spinningdisk/boot


# add luks

cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random --verify-passphrase luksFormat /dev/spinningdisk/root

cryptsetup luksOpen /dev/spinningdisk/root root

mkfs.ext4 /dev/mapper/root


root=/mnt

mount /dev/mapper/root $root

mkdir -p ${root}/boot
mount /dev/spinningdisk/boot ${root}/boot



# debbootstrap


wget --no-check-certificate https://ftp-master.debian.org/keys/archive-key-10.asc 
gpg --import archive-key-10.asc


#mirror=http://mirror.hetzner.de/debian/packages/
mirror=http://ftp.de.debian.org/debian/
packages="makedev lvm2 mdadm grub2 cryptsetup linux-image-amd64 task-ssh-server openssh-server aptitude locales sudo console-setup"
debootstrap --keyring=/root/.gnupg/pubring.kbx --arch amd64 --components "main,contrib,non-free" --include "$packages" buster /mnt/ $mirror 


#cryptsetup luksOpen /dev/mapper/spinningdisk-root root


mkdir -p ${root}/run/lvm
mkdir -p ${root}/run/udev
mkdir -p ${root}/dev

mount --bind /dev $root/dev

mkdir -p ${root}/proc
mount --bind /proc $root/proc


mkdir -p ${root}/sys
mount --bind /sys $root/sys
mount --bind /dev/pts $root/dev/pts
mount --bind /run/lvm $root/run/lvm
mount --bind /run/udev $root/run/udev


LANG=C.UTF-8 chroot /mnt /bin/bash

export TERM=xterm-color
export LC_ALL="en_US.UTF-8"
export PATH="/sbin:/usr/sbin:/usr/local/sbin:/root/bin:/usr/local/bin:/usr/bin:/bin:"$PATH


/usr/sbin/dpkg-reconfigure tzdata


/usr/sbin/dpkg-reconfigure locales



echo "Apt::Install-Recommends 0;" >> /etc/apt/apt.conf.d/local-recommends
apt update

tasksel install standard

mkdir /etc/dropbear-initramfs
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDa3Pm4qBWc4NmWLWfRS4vhpHI4OkphyCjE9nO6JWH+0mvFQlqddjonsl5l7rEBa3vg6kfYoK8DUZD2qhubWhOU5/wkJ10hriS3AsfFIJUXeM3Gb7LdiBQW9vd3J/ThQyTcswuzn8/4Ebbn4uRVxR6uY0UqPpzy0n3YXfz7b8Vj/+coEbfYmdiW1QRyhKjeBTJeX7m9+ofgN8yIQHtw4lZq6btPifTfI3A2ovyH+2lu/up4E2RLb0Yf4WqkHMS9ZQacAHXFXa/pWmvaZIB7NXHhOrGnY+4g7icWWObF+potYjPkPclKa/UxCsGEbjsdr5VUGCpHEX6pp39+0a0z+5VN07D0qNZZKGn9EnYXA4QqUeyHnQc6QSKjBkTwyBDyHW4iboIR1W1Aw3hCAsPSbzIrTfWfrc3j9zjP0dBVJRL6qxfobFmqwGmfit6nWniAYuZyCBBgzQIlMwpK3COdvzZORlH1UQ5d1LjQlbT7Oy4XcE+WQYHbKBo5NNxg1n5YuO7s1bMjB0zR2PC/qpxx0/A458OxqRYoRnEkYQKUYocEM0BSOpxBNPCKejfdvhByOFGmpDh20BYO5YO4d6++NmAIy8hNyidhrKjJF0N8aXHAR6LynCfA6iIxA9rY91qwbPX1w/on0jQKXb/jArx7lrTtFDkGSSfPdVxwmYSd42CRSQ== wonko@puppet" > /etc/dropbear-initramfs/authorized_keys  



apt install -y dropbear-initramfs qemu-kvm libvirt-daemon  bridge-utils virtinst libvirt-daemon-system firmware-linux firmware-linux-free firmware-linux-nonfree

echo root UUID=$(blkid -s UUID -o value /dev/mapper/spinningdisk-root) none luks > /etc/crypttab

echo UUID=$(blkid -s UUID -o value /dev/mapper/spinningdisk-boot) /boot ext4 defaults 0 2 > /etc/fstab
echo UUID=$(blkid -s UUID -o value /dev/mapper/root) / ext4 defaults 0 1 >> /etc/fstab

echo kasterborous > /etc/hostname


mkdir -p /etc/network/interfaces.d
echo 'auto enp1s0' > /etc/network/interfaces.d/enp1s0
echo 'iface enp1s0 inet dhcp' >> /etc/network/interfaces.d/enp1s0


adduser wonko --uid 1073
adduser wonko sudo

mkdir -p /home/wonko/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDa3Pm4qBWc4NmWLWfRS4vhpHI4OkphyCjE9nO6JWH+0mvFQlqddjonsl5l7rEBa3vg6kfYoK8DUZD2qhubWhOU5/wkJ10hriS3AsfFIJUXeM3Gb7LdiBQW9vd3J/ThQyTcswuzn8/4Ebbn4uRVxR6uY0UqPpzy0n3YXfz7b8Vj/+coEbfYmdiW1QRyhKjeBTJeX7m9+ofgN8yIQHtw4lZq6btPifTfI3A2ovyH+2lu/up4E2RLb0Yf4WqkHMS9ZQacAHXFXa/pWmvaZIB7NXHhOrGnY+4g7icWWObF+potYjPkPclKa/UxCsGEbjsdr5VUGCpHEX6pp39+0a0z+5VN07D0qNZZKGn9EnYXA4QqUeyHnQc6QSKjBkTwyBDyHW4iboIR1W1Aw3hCAsPSbzIrTfWfrc3j9zjP0dBVJRL6qxfobFmqwGmfit6nWniAYuZyCBBgzQIlMwpK3COdvzZORlH1UQ5d1LjQlbT7Oy4XcE+WQYHbKBo5NNxg1n5YuO7s1bMjB0zR2PC/qpxx0/A458OxqRYoRnEkYQKUYocEM0BSOpxBNPCKejfdvhByOFGmpDh20BYO5YO4d6++NmAIy8hNyidhrKjJF0N8aXHAR6LynCfA6iIxA9rY91qwbPX1w/on0jQKXb/jArx7lrTtFDkGSSfPdVxwmYSd42CRSQ== wonko@puppet" > /home/wonko/.ssh/authorized_keys

chown -R wonko /home/wonko/.ssh
chmod 700 /home/wonko/.ssh
chmod 600 /home/wonko/.ssh/authorized_keys



sed -ie 's/\(GRUB_CMDLINE_LINUX_DEFAULT=".*\)"/\1 md_mod.start_dirty_degraded=1\"/' /etc/default/grub




update-initramfs -c -k all
update-grub
grub-install ${D1}
grub-install $D2

# rescue hints

/sbin/mdadm --stop md0
/sbin/mdadm --assemble --run md0

/sbin/lvm vgchange -a y





