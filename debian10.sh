#!/usr/bin/env bash

set -x 

if [ -z "$1" ]; then
	echo "1st arg must be fqdn"
	exit 1
else
	FQDN="$1"
fi


IPADDR=$(getent hosts ${FQDN} |cut -d " " -f 1)
if [ -z "$IPADDR" ]; then
	echo "Could not find IP for $FQDN"
	exit 1
fi

preseedfile="$(mktemp -d )/preseed.cfg"
echo "Preparing preseed file in $preseedfile"
cat preseed.cfg.tmpl > $preseedfile
sed -i "s/{{FQDN}}/${FQDN}/" $preseedfile
sed -i "s/{{IPADDR}}/${IPADDR}/" $preseedfile


virt-install --connect qemu:///system \
	--name ${FQDN} \
	--memory 2048 --vcpus 2 \
	--location $HOME/install/debian-10.3.0-amd64-DVD-1.iso \
	--initrd-inject=$preseedfile \
	--extra-args "auto console=ttyS0,115200n8 serial DEBIAN_FRONTEND=text" \
	--os-variant debian10 \
	--graphics none \
	--console pty,target_type=serial \
	--disk size=10 \
	--noreboot \
       	--network network=natted

virsh --connect qemu:///system start ${FQDN}

echo "removing preseed file"
rm $preseedfile